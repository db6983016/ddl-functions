--------1:
CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
WITH current_quarter AS (
    SELECT 
        EXTRACT(YEAR FROM CURRENT_DATE) AS year,
        CEIL(EXTRACT(MONTH FROM CURRENT_DATE) / 3.0) AS quarter
)
SELECT 
    c.name AS category,
    SUM(p.amount) AS total_revenue
FROM
    payment p
    INNER JOIN rental r ON p.rental_id = r.rental_id
    INNER JOIN inventory i ON r.inventory_id = i.inventory_id
    INNER JOIN film_category fc ON i.film_id = fc.film_id
    INNER JOIN category c ON fc.category_id = c.category_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = (SELECT year FROM current_quarter)
    AND CEIL(EXTRACT(MONTH FROM p.payment_date) / 3.0) = (SELECT quarter FROM current_quarter)
GROUP BY 
    c.name
HAVING 
    SUM(p.amount) > 0;


--------2:
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(p_date DATE)
RETURNS TABLE(category VARCHAR, total_revenue NUMERIC) AS $$
    SELECT 
        c.name AS category,
        SUM(p.amount) AS total_revenue
    FROM
        payment p
        INNER JOIN rental r ON p.rental_id = r.rental_id
        INNER JOIN inventory i ON r.inventory_id = i.inventory_id
        INNER JOIN film_category fc ON i.film_id = fc.film_id
        INNER JOIN category c ON fc.category_id = c.category_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = EXTRACT(YEAR FROM p_date)
        AND CEIL(EXTRACT(MONTH FROM p.payment_date) / 3.0) = CEIL(EXTRACT(MONTH FROM p_date) / 3.0)
    GROUP BY 
        c.name
    HAVING 
        SUM(p.amount) > 0;
$$ LANGUAGE sql;


--------3:
CREATE OR REPLACE FUNCTION new_movie(p_title VARCHAR)
RETURNS VOID AS $$
DECLARE
    v_language_id INT;
    v_current_year INT := EXTRACT(YEAR FROM CURRENT_DATE);
BEGIN
    SELECT language_id INTO v_language_id
    FROM language
    WHERE name = 'Klingon';

    IF v_language_id IS NULL THEN
        RAISE EXCEPTION 'Language "Klingon" does not exist in the language table.';
    END IF;

    INSERT INTO film (
        title,
        release_year,
        language_id,
        rental_duration,
        rental_rate,
        replacement_cost,
        last_update
    )
    VALUES (
        p_title,
        v_current_year,
        v_language_id,
        3,
        4.99,
        19.99,
        CURRENT_TIMESTAMP
    );

    RAISE NOTICE 'New movie with title "%" has been inserted successfully.', p_title;
END;
$$ LANGUAGE plpgsql;

